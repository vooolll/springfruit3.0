CREATE TABLE venues
(
  id character varying(255) NOT NULL,
  name character varying(255),
  phone character varying(255),
  address character varying(255),
  lat double precision,
  lng double precision,
  city character varying(255),
  state character varying(255),
  country character varying(255),
  category_id character varying(255),
  category_name character varying(255),
  CONSTRAINT pk_venue_id PRIMARY KEY (id)
);

CREATE TABLE users
(
  id serial NOT NULL,
  email character varying(255) NOT NULL,
  password character varying(255) NOT NULL,
  gender boolean NOT NULL,
  email_key character varying(255),
  role smallint NOT NULL DEFAULT 1,
  status character varying(140),
  device_id text,
  platform smallint,
  image_name character varying(100) NOT NULL,
  mess_count integer DEFAULT 0,
  icon_id character varying(25) DEFAULT 'point'::character varying,
  email_hash character varying(255),
  CONSTRAINT pk_user_id PRIMARY KEY (id),
  CONSTRAINT uk_user_email UNIQUE (email)
);

CREATE TABLE linkups
(
  id serial NOT NULL,
  sender_id integer NOT NULL,
  receiver_id integer NOT NULL,
  sender_decision boolean NOT NULL DEFAULT true,
  receiver_decision boolean NOT NULL DEFAULT false,
  CONSTRAINT pk_linkups_id PRIMARY KEY (id)
)