/**
 * Created by springfruit on 22.07.14.
 */
var dependencies  = require(__dirname + '/app/boot/dependencies');


dependencies.then(function(deps){


	deps.modules.beanstalk.watch('default').onSuccess(function(data){
		console.log("------------CONSUMER CONNECTED-------------");
		deps.modules.beanstalk.reserve().onSuccess(function(job) {
			console.log('reserved', job);
		}).onError(function(err){
			console.log('----------- reserve error ----------');
			console.log(err);
		});
	}).onError(function(err){
		console.log('----------- watch error ----------');
		console.log(err);
	});

}).catch(function(err){
	console.log('----------- deps error ----------');
	console.log(err);
	console.log('----------- deps error ----------');
});