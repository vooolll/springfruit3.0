/**
 * Created by springfruit on 21.05.14.
 */
var model = require('nodejs-model');

var Linkup = model('Linkup')
	.attr('sender_id', {
		'validations': {}
	})
	.attr('receiver_id', {
		'validations': {}
	});

module.exports = Linkup;