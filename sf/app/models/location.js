var model = require('nodejs-model');



var Location = model('Location')
.attr('location', {
	'validations': {
		presence: true
	}
}).attr('user_id', {
	'validations': {}
}).attr('venue_id', {
  'validations': {}
}).attr('category_id', {
  'validations': {}
}).attr('gender', {
  'validations': {}
}).attr('city', {
	'validations': {}
}).attr('image_name', {
	'validations': {}
}).attr('status', {
}).attr('mess_count', {
}).attr('icon', {
}).attr('image_big_link', {
}).attr('image_small_link', {
}).attr('last_appearance', {
}).attr('accept', {
}).attr('email_hash', {
}).attr('email', {
});

module.exports = Location;