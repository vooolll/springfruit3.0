/**
 * Created by springfruit on 21.04.14.
 */
var model = require('nodejs-model');

var User = model('User')
	.attr('id', {
		'validations': {}
	}).attr('email', {
		'validations': {
			'presence': {
				'message': 'email required'
			}
		}
	}).attr('password', {
		'validations': {
//			'length': {
//				'minimum': 8,
//				'maximum': 16,
//				'messages': {
//					'tooShort': 'too short password',
//					'tooLong': 'too long password'
//				}
//			}
		}
	}).attr('gender', {
		'validations': {}
	}).attr('image_name', {
		'validations': {}
	}).attr('status', {
		'validations': {}
	}).attr('device_id', {
		'validations': {}
	}).attr('platform', {
		'validations': {}
	}).attr('mess_count', {
		'validations': {}
	}).attr('icon_id', {
		'validations': {}
	}).attr('image_small_link', {

	}).attr('image_big_link',{

	}).attr('email_hash', {

	}).attr('email_key', {

	});


User.validator('emailRequired', function(model, property, options){
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	console.log(model.email());
	console.log(property);
	console.log(options.message);
	if (re.test(model[property])) {
		model.addError(property, options.message);
	}
});

module.exports = User;