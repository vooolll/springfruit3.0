/**
 * Created by springfruit on 17.04.14.
 */
var model = require('nodejs-model');

var Venue = model('Venue')
  .attr('id', {
    'validations': {}
  }).attr('name', {
    'validations': {}
  }).attr('phone', {
    'validations': {}
  }).attr('address', {
    'validations': {}
  }).attr('lat', {
    'validations': {}
  }).attr('lng', {
    'validations': {}
  }).attr('city', {
    'validations': {}
  }).attr('state', {
    'validations': {}
  }).attr('country', {
    'validations': {}
  }).attr('category_id', {
    'validations': {}
  }).attr('category_name', {
    'validations': {}
  }).attr('total', {
		'validations': {},
		'tags': ['stats']
	}).attr('female', {
		'validations': {},
		'tags': ['stats']
	}).attr('male', {
		'validations': {},
		'tags': ['stats']
	});


module.exports = Venue;