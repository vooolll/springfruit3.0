var auth = require('./middlewares/auth');

module.exports = function(app, controllers) {

	app.get('/api/location', auth, controllers.locations.getLocation);
	app.post('/api/location', auth, controllers.locations.postLocation);
	app.post('/api/signup', controllers.users.signup);
	app.get('/api/stats', auth, controllers.locations.getStats);
	app.get('/api/user/:sid', controllers.users.getByKey);
	app.post('/api/user/status', auth, controllers.users.setStatus);
	app.post('/api/user/changeImage', auth, controllers.users.changeImage);
	app.post('/api/user/forgot', controllers.users.forgot);
	app.post('/api/linkup/add', auth, controllers.linkups.add);
  app.post('/api/checkEmail', controllers.users.checkEmail);
	app.post('/api/user/icon', auth, controllers.users.setAvatar);
	app.post('/api/user/password', auth, controllers.users.changePassword);
	app.get('/api/linkup/decision', auth, controllers.linkups.decision);
	app.get('/api/linkup/delete', auth, controllers.linkups.del);
	app.get('/api/user/:uid/reset/:key', controllers.users.checkKey);
	app.post('/api/user/:uid/reset', controllers.users.reset);
	app.get('/api/user/block/list', auth, controllers.users.blockList);
	app.get('/api/logout', auth, controllers.locations.logout);
	app.get('/api/user/get/:id', controllers.users.getById);
	app.get('/api/user/hash/:hash', controllers.users.getByHash);
	app.post('/api/chat/messages', controllers.users.offlineMessages);
};