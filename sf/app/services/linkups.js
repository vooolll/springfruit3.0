/**
 * Created by springfruit on 21.05.14.
 */
var Linkup = require('../models/linkup');
var when   = require('when');

var LinkupService = function(){
	this.dependencies = {
		'repo': 'repositories.linkups',
		'repoUser': 'repositories.users',
		'push': 'modules.push'
	}
};

LinkupService.prototype.add = function(current, another){
	var self = this;
	var linkup  = Linkup.create();
	linkup.senderId(current);
	linkup.receiverId(another);
	var promise = self.repo.isLinkuped(linkup).then(function(result){

		if (result.allowed) {
			return self.repoUser.getDeviceInfo(linkup.receiverId()).then(function(deviceInfo){
				if (deviceInfo.platform) {
					self.push.ios({
						'deviceToken': deviceInfo.device_id,
						'alert': "You have LinkUp",
						'badge': 1,
						'sound': "sound_long.mp3",
						'content': 1,
						'set': { 'key': 'linkup', 'value': true }
					});
				} else {
					self.push.android(
						{ 'data':
							{
								'text': "Do you agree to LinkUp",
								'id': current,
								'linkup': true
							},
							'collapseKey': 'default',
							'registrationId': deviceInfo.device_id
					});
				}
				return self.repo.add(linkup).then(function(){
					return {'error': false, 'description': 'success'};
				});
			});

		} else {
			if (result.mine) {
				return {'error': true, 'description': 'You have linkup', 'code': 13};
			} else {
				return {'error': true, 'description': 'Another user has linked up', 'code': 14};
			}
		}
	});

	return promise;
};

LinkupService.prototype.accept = function(userId, accept){
	var self = this;

	var promise = self.repo.accept(userId, accept).then(function(linkup){
		console.log(linkup);
		if (linkup) {
			self.repoUser.getDeviceInfo(linkup.sender_id).then(function(deviceInfo){
				console.log(deviceInfo);
				if (accept) {
					alert = "Your LinkUp was accepted";
				} else {
					alert = "Your LinkUp was not accepted";
				}

				if (deviceInfo.platform) {

					var alert;

					self.push.ios({
						'deviceToken': deviceInfo.device_id,
						'alert': alert,
						'badge': 1,
						'sound': "sound_long.mp3",
						'content': 1,
						'set': {'key': 'accept', 'value': accept}
					});
				} else {
					self.push.android(
					{ 'data':
						{
							text: alert,
							accept: accept
						},
						 'collapseKey': 'default',
						'registrationId': deviceInfo.device_id
					});
				}
			});

		} else {

		}

	});

	return promise;
};

LinkupService.prototype.del = function(userId){
	return this.repo.del(userId);
};


module.exports = LinkupService;