/**
 * Created by springfruit on 29.04.14.
 */
var User   = require('../models/user');

var when   = require('when');
var crypto = require('crypto');

var UsersService = function(){

	this.ERROR_USER_EXIST = 4;

	this.errorDescription = {};
	this.errorDescription[this.ERROR_USER_EXIST] = 'User exists';
	this.dependencies   = {
		'repo' : 'repositories.users',
		'email': 'modules.email',
		'locRepo': 'repositories.locations',
		'push' : 'modules.push'
	};

	this.validatePassword = function(pass, repPass){

		var promise = when.promise(function(resolve){
			if (pass === repPass){
				if (pass.length === 8) {
					 resolve({'status': true});
				} else {
					resolve({'status': false, 'code': 2});
				}
			} else {
				resolve({'status': false, 'code': 1});
			}
		});
		return promise;
	}
};

UsersService.prototype.create = function(data){
	var user = User.create(data);
	var self = this;
	var promise = user.validate().then(function(){
		if (user.isValid){
			var prom = self.repo.isEmailExists(user.email()).then(function(isExists){
				if (isExists) {
					return {
						'error': true,'description': self.errorDescription[self.ERROR_USER_EXIST],
						'code': self.ERROR_USER_EXIST
					};

				} else {
					var promFiles = self.repo.processFiles(data.imagesInfo, user.email()).then(function(name){
						user.imageName(name);
						return self.repo.create(user);
					});
					return promFiles;
				}
			});
			return prom;
		} else {
			throw user.errors;
		}
	});

	return promise;
};

UsersService.prototype.getByKey = function(key){
	return this.repo.getByKey(key);
};

UsersService.prototype.setStatus = function(data, user, sid){
	user.status(data.status);

	var self = this;

	var promise = self.repo.setKey(sid, user).then(function(){
		return self.locRepo.update(data, user).then(function(){
			return self.repo.setStatus(data, user.id());
		});
	});

	return promise;
};


UsersService.prototype.setAvatar = function(data, user, sid){
	user.iconId(data.icon);

	var self = this;

	var promise = self.repo.setKey(sid, user).then(function(){
		return self.locRepo.update(data, user).then(function(){
			return self.repo.setAvatar(data.icon, user.id());
		});
	});

	return promise;
};


UsersService.prototype.changeImage = function(data, user, sid){
	var self = this;

	var promFiles = self.repo.processFiles(data.imagesInfo, user.email()).then(function(name){
		return self.repo.updateImageName(name, user.id()).then(function(images){

			user.imageName(name);
			user.imageSmallLink(images.small);
			user.imageBigLink(images.big);

			return self.repo.setKey(sid, user).then(function(){
				return self.locRepo.update(images, user).then(function(){
					return images;
				});
			});
		});
	});
	return promFiles;
};

UsersService.prototype.forgotPassword = function(email){
	var self = this;

	var promise = self.repo.createEmailKey(email).then(function(keyWithId){
		var messageInfo = { 'templateName': 'forgot', 'attachToMessage': {'key': keyWithId.key , 'id' : keyWithId.id}, subject: 'Forgot password'};
		return self.email.send(email, messageInfo);
	});

	return promise;
};

UsersService.prototype.checkEmail = function(email){
	var promise = this.repo.isEmailExists(email).then(function(isEx){
		return isEx;
	});
	return promise;
};


UsersService.prototype.changePassword = function(data, userId){
	var self = this;
	var md5HashOld = crypto.createHash('md5').update('Zhanserik' + data.old_password).digest('hex');
	var hashedOldPassword = crypto.createHash('md5').update(md5HashOld).digest('hex');

	var md5HashNew = crypto.createHash('md5').update('Zhanserik' + data.new_password).digest('hex');
	var hashedNewPassword = crypto.createHash('md5').update(md5HashNew).digest('hex');

	var promise =	self.repo.isPasswordMatch(hashedOldPassword, userId).then(function(isMatch){
		if (isMatch) {
			return self.repo.changePassword(hashedNewPassword, userId).then(function(){
				return {'error': false, 'description': 'success'};
			});
		} else {
			return {'error': true, 'description': 'wrong password', 'code': 2};
		}
	});
	return promise;
};



UsersService.prototype.checkKey = function(key, id){
	var self = this;

	var promise = self.repo.getById(id).then(function(user){
		return user.emailKey() === key;
	});

	return promise;
};

UsersService.prototype.reset = function(data, id){
	var self = this;
	var promise = self.validatePassword(data.password, data.passwordRepeat).then(function(result){
		return self.repo.reset(data, id).then(function(){
			if (result.status){
				return { 'message': 'Success', 'description': 'Welcome back to SpringFruit'};
			} else if(!result.status && result.code === 1){
				return { 'message': 'OOPS', 'description': 'Password does not match'};
			} else if (!result.status && result.code === 2){
				return { 'message': 'OOPS','description': 'Password should be more than 8 symbols'};
			}
		});
	});
	return promise;
};

UsersService.prototype.blockList = function(user){
	var self = this;
	return self.repo.getBlockListFor(user);
};


UsersService.prototype.getById = function(id){
	var self = this;
	return self.repo.getById(id);
};

UsersService.prototype.getByHash = function(hash){
	var self = this;
	return self.repo.getByHash(hash);
};

UsersService.prototype.sendOffline = function(data){
	var self = this;
	console.log(data);

	return self.repo.getDeviceInfoByHash(data.to).then(function(deviceInfo){
		if (deviceInfo.platform) {
			self.push.ios({
				'deviceToken': deviceInfo.device_id,
				'alert': "You have new message",
				'badge': 1,
				'sound': "sound_long.mp3",
				'content': 1,
				'set': { 'key': 'senderId', 'value': data.from }
			});
		} else {
			self.push.android({ 'data': {
				text: "You have offline message",
				body: data.body
				},
				'collapseKey': 'default',
				'registrationId': deviceInfo.device_id
			});
		}
	});

};

module.exports = UsersService;