var Location    = require('../models/location');
var amqp        = require('amqp');
var when        = require('when');


var LocationsService    = function(){

	this.dependencies   = {
		'repo'      : 'repositories.locations',
		'venueRepo' : 'repositories.venues',
		'userRepo'  : 'repositories.users',
		'linkupRepo': 'repositories.linkups',
		'cloudfront': 'modules.cloudfront'
	};

	this.merge = function (models, linkup) {


		var self = this;

		var promise = when.promise(function(resolve, reject){

			if (typeof linkup === 'object' && linkup.length === undefined){
				var foundInElastic = false;
				models.forEach(function (item) {
					if (parseInt(item.userId(), 10) === linkup.id){
						item.accept(linkup.accept);
						foundInElastic = true;
					}
				});

				if (!foundInElastic){
					return self.userRepo.getById(linkup.id).then(function(user){
						var loc = Location.create();

						loc.userId(user.id());
						loc.gender(user.gender());
						loc.imageSmallLink(self.cloudfront.host + "/images/small/" + user.imageName());
						loc.imageBigLink(self.cloudfront.host + "/images/big/" + user.imageName());
						loc.status(user.status());
						loc.messCount(user.messCount());
						loc.email(user.email());
						loc.accept(linkup.accept);
						models.push(loc);
						resolve(models);
					}).catch(function(err){
						console.log(err);
						reject(err);
					});
				}
			} else {
				resolve(models);
			}


			resolve(models);
		});



		return promise;
	};
};

LocationsService.prototype.save = function(data, user){

	var loc     = Location.create(data);
	var self    = this;

	loc.userId(user.id());
	loc.gender(user.gender());
	loc.imageName(user.imageName());
	loc.status(user.status());
	loc.messCount(user.messCount());
	loc.icon(user.iconId());
	loc.emailHash(user.emailHash());
	loc.email(user.email());
	loc.lastAppearance(Date.now() / 1000);

	console.log('----------Location model-------------');
	console.log(loc);
	console.log('-------------------------------------');

	var promise = loc.validate().then(function(){
		if(loc.isValid) {
			var prom = self.userRepo.updateDeviceInfo(user.id(), data.device_id, data.platform).then(function(){
				return self.repo.save(loc);
			});
			return prom;
		} else {
			throw loc.errors;
		}
	});

	self.venueRepo.getById(data.venue_id).then(function(venue){
		if (venue) {
			console.log('--------Venue already exists---------');
			console.log(venue);
			console.log('-------------------------------------');
		} else {
			self.venueRepo.getVenueInfo(data.venue_id, function(err, venue){
				if (err) {
					console.error(err);
				}	self.venueRepo.save(venue,function(err, venue){
					if (err) {
						console.error(err);
					} else {
						console.log('-----Venue retrieved------------');
						console.log(venue);
						console.log('--------------------------------');
					}
				});
			});
		}
	});

	return promise;
};

LocationsService.prototype.getLocation = function(data, userId) {
	var self = this;


	var promise = self.linkupRepo.linkupFor(userId).then(function(linkupId){

		return self.userRepo.getBlocked(userId).then(function(blockedIds){
			return self.repo.getNear(data.lat, data.lng,  blockedIds, linkupId).then(function(locations){

				return self.linkupRepo.getLinkup(userId).then(function(linkups){
					console.log('---------Linkup user id---------');
					console.log(linkupId);
					console.log('--------------------------------');

					console.log('---------Blocked user id---------');
					console.log(blockedIds);
					console.log('---------------------------------');

					console.log('---------User id-----------');
					console.log(userId);
					console.log('---------------------------');
					return self.userRepo.getMessageCounters(locations).then(function(locsWithCount){
						console.log("getMessageCounters");
						return self.merge(locsWithCount, linkups);
					});
				});
			});
		});
	});

	return promise;
};


LocationsService.prototype.getStats = function(city) {
	var self = this;

	var start = new Date().getTime();
	var promise = self.repo.getStats(city).then(function(data){
		var end = new Date().getTime();
		console.log("TIME SPEND ON STATS");
		console.log((end - start) / 1000);

		var ids = [];
		for (var i in data) {
			ids.push(data[i].venue_id);
		}



		var prom = self.venueRepo.getById(ids).then(function(venueStats){
			var prepared	= {};

			if (venueStats){
				data.forEach(function(d){
					prepared[d.venue_id]	= d;
				});

				venueStats.forEach(function(venue){
					if(prepared[venue.id()]){
						venue.total(prepared[venue.id()].total);
						venue.male(prepared[venue.id()].male);
						venue.female(prepared[venue.id()].female);
					}
				});

				console.log('-----------City------------');
				console.log(city);
				console.log('---------------------------');

				console.log('-----------Stat-------------');
				console.log(venueStats.toJSON('*'));
				console.log('----------------------------');

				return venueStats.toJSON('*');
			} else {
				return [];
			}


		});

		return prom;
	});

	return promise;
};




LocationsService.prototype.logout = function(user, sid){
	var self = this;
	console.log('---------User logged out-----------');
	console.log(user);
	console.log('-----------------------------------');
	var promise = self.repo.del(user).then(function(){
		return self.userRepo.deleteByKey(sid);
	});

	return promise;
};

module.exports  = LocationsService;