/**
 * Created by springfruit on 17.04.14.
 */

var Venue    = require('../models/venue');

var VenuesService    = function() {
  this.dependencies   = {
    'repo'  : 'repositories.venues'
  };
};

VenuesService.prototype.save = function(data, callback) {
  var self = this;

	console.log(data);

  var ven = Venue.create();

	console.log(data);

  ven.id(data.id);
  ven.name(data.name);
  //это с foursquare в таком виде приходит
  ven.phone(data.contact.formattedPhone);
  ven.address(data.location.address);
  ven.lat(data.location.lat);
  ven.lng(data.location.lng);
  ven.city(data.location.city);
  ven.state(data.location.state);
  ven.country(data.location.country);
  ven.categoryId(data.categories[0].id);
  ven.categoryName(data.categories[0].name);



  ven.validate().then(function() {
    if (ven.isValid) {
      self.repo.save(ven, function(err, venue) {
        if (err) {
          callback(err);
        } else {
          callback(null, venue);
        }
      });
    } else {
      callback(ven.errors);
    }
  });
};

VenuesService.prototype.getById = function(id, callback) {
  this.repo.getById(id, function(err, venue) {
    if (err) {
      callback(err);
    } else {
      callback(null, venue);
    }
  });
};

VenuesService.prototype.getVenueInfo = function(id, callback) {
	this.repo.getVenueInfo(id, function(err,  venue) {
		if (err) {
			callback(err);
		} else {
			callback(null, venue);
		}
	});
};

module.exports = VenuesService;