/**
 * Created by springfruit on 12.05.14.
 */

exports.parse = function(arr) {
	arr[0] = arr[0].replace(/}/g, "");
	arr[0] = arr[0].replace(/{/g, "");
	arr[0] = arr[0].replace(/\n/g, "");
	arr[0] = arr[0].replace(/[\[\]']+/g, "");
//	arr[0] = arr[0].replace(/"/g, "");
	arr[0] = arr[0].replace(/</g, "");
	arr[0] = arr[0].replace(/>/g, "");
	arr[0] = arr[0].replace(/selected,/g, "");
	arr[0] = arr[0].trim();

	arr[1] = arr[1].replace(/}/g, "");
	arr[1] = arr[1].replace(/{/g, "");
	arr[1] = arr[1].replace(/\n/g, "");
	arr[1] = arr[1].replace(/[\[\]']+/g, "");
//	arr[1] = arr[1].replace(//g, "");
	arr[1] = arr[1].replace(/</g, "");
	arr[1] = arr[1].replace(/>/g, "");
	return arr;
};

exports.parseStr = function(arr) {

	for (var i in arr) {
		arr[i] = arr[i].replace(/"/g, "");
	}

	return arr;
};