var Collection  = function() {

	this.toJSON = function(tag) {
		var result  = [];
		this.forEach(function(el){
			result.push(el.toJSON(tag));
		});

		return result;
	}
};

Collection.prototype    = new Array();



module.exports  = Collection;