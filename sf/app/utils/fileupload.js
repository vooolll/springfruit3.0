/**
 * Created by springfruit on 16.05.14.
 */


exports.getFile = function(request) {
	var data = {};
	data.imagesInfo = {};
	request.pipe(request.busboy);
	request.busboy.on('file', function(fieldname, file, filename, encoding) {
		file.on('data', function(binary) {
			data.imagesInfo[fieldname + 'Encoding'] = encoding;
			data.imagesInfo[fieldname] = binary;
		});
	});

	request.busboy.on('finish', function() {
		return data;
	});
};


exports.processMultiparty = function(request) {
	var data = {};
	data.imagesInfo = {};
	request.pipe(request.busboy);
	request.busboy.on('file', function(fieldname, file, filename, encoding) {
		file.on('data', function(binary) {
			data.imagesInfo[fieldname + 'Encoding'] = encoding;
			data.imagesInfo[fieldname] = binary;
		});
	});
	request.busboy.on('field', function(fieldname, val) {
		data[fieldname] = val;
	});

	request.busboy.on('finish', function() {
		return data;
	});
};