var path    = require('path');
var fs      = require('fs');
var Queue   = require('priorityqueuejs');
var dotty   = require('dotty');


exports.attachDependencies = function(obj, dependecies) {

	var keys    = Object.keys(obj.dependencies);
	keys.forEach(function(key) {
		var depKey  = obj.dependencies[key];
		var depObj  = dotty.get(dependecies, depKey);

		obj[key]    = depObj;
	});
};

exports.requireDir = function(dir, priorities) {
	var priorities  = priorities || {};
	var files       = fs.readdirSync(dir);
	var qu          = new Queue(function(a, b) {
		return a.priority - b.priority;
	});


	files.forEach(function(name){
		var obj = require(dir + '/' + name);
		qu.enq({
			'data'      : {
				'file'     : name,
				'key'      : path.basename(name, '.js'),
				'obj'      : obj
			},
			'priority'  : priorities[name] || 0
		});
	});

	var objects = [];
	while(qu.size()) {
		var obj = qu.deq();
		objects.push(obj.data);
	}

	return objects;
};
