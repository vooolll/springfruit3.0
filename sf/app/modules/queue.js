//var amqp = require('amqp');



//
//var hook    = function(config) {
//
//	if(!config.amqp ||
//		!config.amqp.host ||
//		!config.amqp.port) {
//		throw new Error('wrong db conf');
//	}
//
//	var manager = function() {
//		var self        = this;
//		var queues      = {};
//		var connection  = amqp.createConnection({
//			host    : config.amqp.host,
//			port    : config.amqp.port
//		});
//
//
//		connection.on('ready', function () {
//			var exchange    = connection.exchange('main');
//
//			console.log('----------------');
//			console.log(exchange);
//			console.log('----------------');
//		});
//
//
////		connection.exchange("my_exchange"
//
//
//
//		var getQueue    = function(name) {
//			if(!queues[name]) {
//				queues[name]    = connection.queue('my-queue', function (queue) {
//					console.log('Queue ' + queue.name + ' is open');
//				});
//			}
//
//			return queues[name];
//		}
//
//		this.push   = function(queueName, data) {
////			exchange.publish('', { message: data });
//		}
//
//	};
//
//	return new manager();
//};
//
////module.exports  = hook;
//module.exports  = null;

//var amqp    = require('amqp');
//var when    = require('when');
//var fn      = require('when/node/function');
//
//var hook    = function(config, callback) {
//	if(!config.amqp ||
//		!config.amqp.host ||
//		!config.amqp.port) {
//		throw new Error('wrong db conf');
//	}
//
//
//	var promise = when.promise(function(resolve, reject, notify) {
////		var manager = {
////			connection : amqp.createConnection({
////				host    : config.amqp.host,
////				port    : config.amqp.port
////			}),
////
////			send : function(messsage)
////			{
////
////			}
////		};
//
//		var connection  = amqp.createConnection({
//			host    : config.amqp.host,
//			port    : config.amqp.port
//		});
//
//		connection.on('ready', function () {
//			var exchange    = connection.exchange('main');
//		});
//
//
//	});
//
//
//
//
//
////	var connection  = amqp.createConnection({
////		host    : config.amqp.host,
////		port    : config.amqp.port
////	});
//
//	connection.on('ready', function(){
//		callback(null, connection);
//	});
//};


//module.exports = fn.lift(hook);
//module.exports  = hook;


var amqp = require('amqplib');
var when = require('when');

var hook = function(config) {
	if(!config.amqp ||
		!config.amqp.host ||
		!config.amqp.port) {
		throw new Error('wrong db conf');
	}

	var promise = when.promise(function(resolve, reject, notify) {
		var connString  = config.amqp.host + ':' + config.amqp.port;
		amqp.connect(connString).then(function(conn) {
			conn.createChannel().then(function(ch) {
				var q   = 'task_queue';
				var ok  = ch.assertQueue(q, {durable: true});

				return ok.then(function() {
					var msg = process.argv.slice(2).join(' ') || "Hello World!";
					ch.sendToQueue(q, new Buffer(msg), {deliveryMode: true});
					console.log(" [x] Sent '%s'", msg);
					return ch.close();
				});
			});


//			return when(conn.createChannel().then(function(ch) {
//				var q = 'task_queue';
//				var ok = ch.assertQueue(q, {durable: true});
//
//				return ok.then(function() {
//					var msg = process.argv.slice(2).join(' ') || "Hello World!";
//					ch.sendToQueue(q, new Buffer(msg), {deliveryMode: true});
//					console.log(" [x] Sent '%s'", msg);
//					return ch.close();
//				});
//			})).ensure(function() { conn.close(); });
		});
	});



//	amqp://guest:guest@localhost:5672
//	var queue   = function() {
//		this.push = function(queueName, data) {
//
//		}
//
//		this.process = function(queueName, callback) {
//
//		}
//	}


}

//amqp.connect('amqp://localhost').then(function(conn) {
//	return when(conn.createChannel().then(function(ch) {
//		var q = 'task_queue';
//		var ok = ch.assertQueue(q, {durable: true});
//
//		return ok.then(function() {
//			var msg = process.argv.slice(2).join(' ') || "Hello World!";
//			ch.sendToQueue(q, new Buffer(msg), {deliveryMode: true});
//			console.log(" [x] Sent '%s'", msg);
//			return ch.close();
//		});
//	})).ensure(function() { conn.close(); });
//}).then(null, console.warn);




//var amqp = require('amqplib');
//
//amqp.connect('amqp://localhost').then(function(conn) {
//	process.once('SIGINT', function() { conn.close(); });
//	return conn.createChannel().then(function(ch) {
//		var ok = ch.assertQueue('task_queue', {durable: true});
//		ok = ok.then(function() { ch.prefetch(1); });
//		ok = ok.then(function() {
//			ch.consume('task_queue', doWork, {noAck: false});
//			console.log(" [*] Waiting for messages. To exit press CTRL+C");
//		});
//		return ok;
//
//		function doWork(msg) {
//			var body = msg.content.toString();
//			console.log(" [x] Received '%s'", body);
//			var secs = body.split('.').length - 1;
//			//console.log(" [x] Task takes %d seconds", secs);
//			setTimeout(function() {
//				console.log(" [x] Done");
//				ch.ack(msg);
//			}, secs * 1000);
//		}
//	});
//}).then(null, console.warn);







//
//var connection  = amqp.createConnection({ host: 'dev.rabbitmq.com' });
//
//// Wait for connection to become established.
//connection.on('ready', function () {
//	// Use the default 'amq.topic' exchange
//	connection.queue('my-queue', function(q){
//		// Catch all messages
//		q.bind('#');
//
//		// Receive messages
//		q.subscribe(function (message) {
//			// Print messages to stdout
//			console.log(message);
//		});
//	});
//});
