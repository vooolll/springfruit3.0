/**
 * Created by springfruit on 13.05.14.
 */
var knox = require('knox');
var when = require('when');

var hook = function(config){
	var s3 = knox.createClient({
		'key': config.s3.key,
		'secret': config.s3.secret,
		'bucket': config.s3.bucket
	});

	var promise = when.promise(function(reslove){
		reslove(s3);
	});

	return promise;
};

module.exports = hook;