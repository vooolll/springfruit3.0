/**
 * Created by springfruit on 20.05.14.
 */
var when           = require('when');
var nodemailer     = require('nodemailer');
var emailTemplates = require('email-templates');
var path           = require('path');

var hook = function(config){

	var templatesDir   = path.resolve(__dirname, '..', 'templates');

	var smtpTrasport = nodemailer.createTransport(config.email.protocol, {
		'host': config.email.host,
		'secureConnection': config.email.secureConnection,
		'port': config.email.port,
		'auth': {
			'user': config.email.auth.user,
			'pass': config.email.auth.pass
		}
	});

	var email = {
		'send' : function(emailToSend, messageInfo) {
			var promise = when.promise(function(resolve, reject){

				emailTemplates(templatesDir, function(err, template){
					if (err) {
						reject(err);
					} else {
						template(messageInfo.templateName, messageInfo.attachToMessage, function(err, html){
							if(err) {
								reject(err);
							}
							else {
								console.log(emailToSend);
								smtpTrasport.sendMail({
									'from'      : 'SpringFruit <support@springfruit.com>',
									'to'        : emailToSend.email,
									'subject'   : messageInfo.subject,
									'html'      : html
								}, function(err, responseStatus){
									if (err) {
										reject(err);
									} else {
										resolve(responseStatus);
									}
								});
							}
						});
					}
				});
			});

			return promise;
		}
	};
	
	return email;
};

module.exports = hook;