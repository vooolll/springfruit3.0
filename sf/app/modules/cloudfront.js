/**
 * Created by springfruit on 02.06.14.
 */
var when = require('when');

var hook = function(config){
	var cloudFront = {};
	cloudFront.host = config.cloudfront.host;
	var promise = when.promise(function(resolve){
		resolve(cloudFront);
	});
	return promise;
};

module.exports = hook;