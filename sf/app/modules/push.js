/**
 * Created by springfruit on 04.06.14.
 */
var apnagent = require('apnagent');
var join     = require('path').join;
var when     = require('when');
var gcm      = require('node-gcm');



var hook = function(config){

	var Push = function(){
		this.sender = new gcm.Sender(config.push.android.senderId);

		this.agent = new apnagent.Agent();
		this.agent
			.set('cert file', config.push.ios.cert)
			.set('key file', config.push.ios.key);

		this.agent
			.set('expires', '1d')
			.set('reconnect delay', '1s')
			.set('cache ttl', '30m');

		this.agent.on('message:error', function (err, msg) {
			console.error(err);
		});



		this.agent.connect(function (err) {
			if (err) console.error(err);
			console.log('[%s] apn agent running', 'dev-sandbox');
		});
	};

	Push.prototype.ios = function(message){
		var self = this;
		console.log(message.deviceToken);
		var promise = when.promise(function(resolve, reject){
			self.agent.createMessage()
				.device(message.deviceToken)
				.alert(message.alert)
				.badge(message.badge)
				.sound(message.sound)
				.set(message.set.key, message.set.value)
				.contentAvailable(message.content)
				.send(function (err) {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				});
		});

		return promise;
	};

	Push.prototype.android = function(message){

		var self = this;

		console.log(message);

		var pushMessage = new gcm.Message({
			collapse_key: message.collapseKey,
			data: message.data
		});

		var registrationIds = [];
		console.log(message.registrationId);
		registrationIds.push(message.registrationId);
		console.log(registrationIds);

		var promise = when.promise(function(resolve, reject){

			self.sender.sendNoRetry(pushMessage, registrationIds, function(err, result) {
				if (err) {
					reject(err);
				} else {
					console.log(result)
					resolve();
				}
			});
		});

		return promise;
	};



	return new Push();
};


module.exports = hook;