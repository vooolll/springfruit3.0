/**
 * Created by springfruit on 21.04.14.
 */
var redisDB  = require('redis');
var when    = require('when');

var hook   = function(config) {
	var redis = redisDB.createClient();

	var promise = when.promise(function(resolve){
		resolve(redis);
	});

	return promise;
};

module.exports = hook;