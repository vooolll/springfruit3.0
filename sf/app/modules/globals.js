/**
 * Created by springfruit on 12.06.14.
 */
var when = require('when');

var hook = function(config){
	var globals = {};
	globals.host = config.globals.host;
	var promise = when.promise(function(resolve){
		resolve(globals);
	});
	return promise;
};

module.exports = hook;