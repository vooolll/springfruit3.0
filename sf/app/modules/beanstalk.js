/**
 * Created by springfruit on 22.07.14.
 */
var bs = require('nodestalker');
var when = require('when');

var hook = function(config){
	var promise = when.promise(function(resolve){
	var client = bs.Client();
		resolve(client);
	});

	return promise;
};

module.exports = hook;