var query   = require('pg-query');
var when    = require('when');
var fn      = require('when/node/function');

var hook    = function(config){

	if(!config.db ||
			!config.db.dbname ||
			!config.db.dbuser ||
			!config.db.dbpass ||
			!config.db.dbhost ||
			!config.db.dbport ) {
		throw new Error('wrong db conf');
	}

	var conn  = 'postgres://'
					+ config.db.dbuser
					+ ':'
					+ config.db.dbpass
					+ '@'
					+ config.db.dbhost
					+ ':'
					+ config.db.dbport
					+ '/'
					+ config.db.dbname;

	query.connectionParameters  = conn;

	var db  = {
		execute : function(queryString, parameters, callback) {

			callback = callback || function(){};
			query(queryString, parameters, function(err, rows, result) {
				if (err) {
					callback(err);
				} else {
					callback(null, rows);
				}
			});
		}
	};

	var promise = when.promise(function(resolve, reject, notify) {
		resolve(db);
	});

	return promise;
};

//module.exports  = fn.lift(hook);
module.exports  = hook;

//module.exports  = hook;