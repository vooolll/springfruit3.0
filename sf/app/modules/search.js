var elasticsearch   = require('elasticsearch');
var fn              = require('when/node/function');
var when            = require('when');

var hook    = function(config) {
	var elastic = new elasticsearch.Client({
		host    : config.elastic.host
//		log     : 'trace'//		log     : 'trace'
	});


	var promise = when.promise(function(resolve, reject, notify) {
		resolve(elastic);
	});

	return promise;
};

module.exports  = hook;