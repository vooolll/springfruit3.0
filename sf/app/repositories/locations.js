var when = require('when');
var location = require(__dirname + '/../models/location');
var Collection = require(__dirname + '/../utils/collection');


var LocationsRepository = function () {
	this.dependencies = {
		'elastic': 'modules.search',
		'cloudfront': 'modules.cloudfront',
		'linkupRepo': 'repositories.linkups',
		'userRepo': 'repositories.users'
	};

	this.map = function (json) {
		var self = this;

		var result = new Collection();
		if (json.hits.total) {
			json.hits.hits.forEach(function (data) {

				var loc = location.create(data._source);
				loc.imageSmallLink(self.cloudfront.host + "/images/small/" + loc.imageName());
				loc.imageBigLink(self.cloudfront.host + "/images/big/" + loc.imageName());
				result.push(loc);
			});
		}
		return result;
	};

	this.merge = function (models, linkup) {

		models.forEach(function (item) {
			if (parseInt(item.userId(), 10) === linkup.id) {
				item.accept(linkup.accept);
			}
		});

		return models;
	};
};

LocationsRepository.prototype.save = function (location) {
	var data = location.toJSON();
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.elastic.index(
			{
				index: 'locations',
				type: 'location',
				id: location.userId(),
				body: data
			},
			function (err, response) {
				if (err) {
					reject(err);
				} else {
					resolve(response);
				}
			}
		);
	});

	return promise;
};

LocationsRepository.prototype.getNear = function (lat, lng, idsBlock, idLinkup) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {


		self.elastic.search({
			'index': 'locations',
			'type': 'location',
			'size': 1000000,
			'body': {
				'query': {
					'match_all': {}
				},
				'filter': {
					'and': [
						{
							'or': [
								{
									'geo_distance': {
										'distance': '40km',
										'location': {
											'lat': lat,
											'lon': lng
										}
									}
								},
								{
									'ids': {
										'values': [idLinkup]
									}
								}
							]
						},
						{
							'range': {
								'last_appearance': {
									'gte': Date.now() / 1000 - 3 * 60 * 60
								}
							}
						},
						{
							'not': {
								'filter': {
									'ids': {
										'values': idsBlock
									}
								}
							}
						}
					]
				}
			}
		}, function (err, res) {
			if (err) {
				console.log("ELASTIC FAULT");
				console.log(err);
				reject(err);
			} else {
				resolve(self.map(res));
			}
		});
	});

	return promise;
};


LocationsRepository.prototype.getStats = function (city) {
	var self = this;



	var promise = when.promise(function (resolve, reject) {
		self.elastic.search({
			'index': 'locations',
			'type': 'location',
			'size': 0,
			'body': {
				"size" : 0,
				"aggregations" : {
					"cities" : {
						"filter" : {
							"term" : {
								"city": city
							}
						},
						"aggregations": {
							"lasts" : {
								"filter" : {
									"range" : {
										"last_appearance" : {
											"gte" : Date.now() / 1000 - 3 * 60 * 60
										}
									}
								},
								"aggregations": {
									"venues" : {
										"terms" : { "field" : "venue_id"},
										"aggregations": {
											"genders" : {
												"terms" : {"field" : "gender"}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}, function (err, res) {
			if (err) {
				reject(err);
			} else {
				var data = [];
				res.aggregations.cities.lasts.venues.buckets.forEach(function (venueBucket) {

					var stat = {
						'venue_id': venueBucket.key,
						'total': venueBucket.doc_count,
						'male': 0,
						'female': 0
					};


					venueBucket.genders.buckets.forEach(function (genderBucket) {


						if (genderBucket.key === 'T') {
							stat.male = genderBucket.doc_count;
						} else if (genderBucket.key === 'F') {
							stat.female = genderBucket.doc_count;
						}
					});

					data.push(stat);
				});

				resolve(data);
			}
		});
	});

	return promise;
};


LocationsRepository.prototype.update = function (data, user) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.elastic.update({
			'index': 'locations',
			'type': 'location',
			'id': user.id(),
			'body': {
				'doc': data
			}
		}, function (err, res) {
			if (err) {
				reject(err);
			} else {
				resolve(res);
			}
		});
	});

	return promise;
};


LocationsRepository.prototype.del = function (user) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.elastic.delete({
			'index': 'locations',
			'type': 'location',
			'id': user.id()
		}, function (err, res) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
	return promise;
};


module.exports = LocationsRepository;