/**
 * Created by springfruit on 21.05.14.
 */
var when = require('when');

var LinkupRepository = function(){
	this.dependencies = {
		'db': 'modules.db'
	}
};


LinkupRepository.prototype.add = function(linkup){
	var self = this;

	var promise = when.promise(function(resolve, reject){
		self.db.execute('INSERT INTO linkups (' +
				'sender_id, receiver_id, sender_decision, receiver_decision) VALUES ($1, $2, $3, $4)',
			[linkup.senderId(), linkup.receiverId(), true, false], function(err){
				if (err){
					reject(err);
				} else {
					resolve(linkup);
				}
			});
	});
	return promise;
};


LinkupRepository.prototype.isLinkuped = function(linkup){
	var self = this;

	var promise = when.promise(function(resolve, reject){
		self.db.execute('SELECT id, receiver_decision, sender_decision, sender_id, receiver_id' +
			' FROM linkups WHERE (sender_id=$1 OR receiver_id=$1) OR (sender_id=$2 OR receiver_id=$2)',
			[linkup.senderId(), linkup.receiverId()], function(err, rows){
				if (err){
					reject(err);
				} else if (rows && rows.length > 0){
					if (rows[0].receiver_decision && rows[0].sender_decision) {
						var result;
						if (rows[0].sender_id === parseInt(linkup.senderId(), 10) || rows[0].receiver_id === parseInt(linkup.senderId(), 10)){
							console.log("TRUE");
							result = { 'allowed': false, 'mine': true};
						} else {
							console.log("FALSE");
							result = { 'allowed': false, 'mine': false};
						}
						resolve(result);
					} else if (!rows[0].receiver_decision || !rows[0].sender_decision){
						self.db.execute('DELETE FROM linkups WHERE id=$1', [rows[0].id], function(err){
							if (err) {
								reject(err);
							} else {
								resolve({ 'allowed': true });
							}
						});
					} else {
						resolve({ 'allowed': true });
					}
				} else {
					resolve({ 'allowed': true });
				}
			});
	});

	return promise;
};

LinkupRepository.prototype.getLinkup = function(userId){
	var self = this;

	var promise = when.promise(function(resolve, reject){
		self.db.execute('SELECT * FROM linkups WHERE receiver_id=$1 OR sender_id=$1', [userId], function(err, rows){
			if (err){
				reject(err);
			} else {
				var data = {};
				if (rows && rows.length !== 0) {
					if (rows[0].receiver_decision && rows[0].sender_decision){
						data['accept'] = true;
						if (rows[0].receiver_id !== parseInt(userId, 10)){
							data['id'] = rows[0].receiver_id;
						} else {
							data['id'] = rows[0].sender_id;
						}
						resolve(data);
					} else {
						data['accept'] = false;
						if (rows[0].receiver_id === parseInt(userId, 10)){
							data['id'] = rows[0].sender_id;
							resolve(data);
						} else {
							resolve([]);
						}
					}
				} else {
					resolve([]);
				}
			}
		});
	});

	return promise;
};


LinkupRepository.prototype.linkupFor = function(userId){
	var self = this;
	var promise = when.promise(function(resolve, reject){
		self.db.execute('SELECT receiver_id, sender_id FROM linkups WHERE (receiver_id=$1 OR sender_id=$1)', [userId],
			function(err, rows){
				if (err) {
					reject(err);
				} else {
					if (rows && rows.length !== 0) {
						if (rows[0].receiver_id === parseInt(userId, 10)) {
							resolve(rows[0].sender_id);
						} else if (rows[0].sender_id === parseInt(userId, 10)){
							resolve(rows[0].receiver_id);
						} else {
							resolve([]);
						}
					} else {
						resolve([]);
					}
				}
		});
	});
	return promise;
};

LinkupRepository.prototype.accept = function(userId, accept){
	var self = this;
	var accept = accept == "true";

	var promise = when.promise(function(resolve, reject){
		if (accept){
			self.db.execute('UPDATE linkups SET receiver_decision=$1 WHERE receiver_id=$2 RETURNING sender_id AS sender_id',
				[accept, userId], function(err, rows){
				if (err) {
					reject(err);
				} else {
					resolve(rows[0]);
				}
			});
		} else {
			self.db.execute('DELETE FROM linkups WHERE receiver_id=$1 RETURNING sender_id AS sender_id',
				[userId], function(err, rows){
				if (err){
					reject(err);
				} else {
					resolve(rows[0]);
				}
			});
		}
	});

	return promise;
};


LinkupRepository.prototype.del = function(userId){
	var self = this;

	var promise = when.promise(function(resolve, reject){
		self.db.execute('DELETE FROM linkups WHERE (sender_id=$1 OR receiver_id=$1)', [userId], function(err){
			if (err){
				reject(err);
			} else {
				resolve();
			}
		});
	});

	return promise;
};

module.exports = LinkupRepository;