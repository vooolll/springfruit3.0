/**
 * Created by springfruit on 21.04.14.
 */
var when         = require('when');
var fs           = require('fs');
var path         = require('path');
var crypto       = require('crypto');
var strutil      = require('../utils/strutil');
var nodefn       = require('when/node');
var User         = require('../models/user');
var randomString = require('random-string');


var promiseWriteFile = nodefn.lift(fs.writeFile);


var UsersRepository = function () {
	this.dependencies = {
		'redis': 'modules.redis',
		'db': 'modules.db',
		's3': 'modules.s3',
		'cloudfront': 'modules.cloudfront',
		'globals': 'modules.globals'
	};

	this.mapToJSON = function (arrayOfArrays) {
		var jsonObject = {};
		for (var i = 0; i < arrayOfArrays[0].length; i++) {
			jsonObject[arrayOfArrays[0][i]] = arrayOfArrays[1][i];
		}
		return jsonObject;
	};

	this.map = function (json) {
		var user = User.create(json);


		if ((typeof user.gender() === 'string')) {
			if (user.gender() === 't') {
				user.gender(true);
			} else {
				user.gender(false);
			}
		} else {

		}
		user.imageBigLink(this.cloudfront.host + '/images/big/' + user.imageName());
		user.imageSmallLink(this.cloudfront.host + '/images/small/' + user.imageName());
		return user;
	};

	this.parseHashes = function (hashes) {
		var parsedHashes = [];

		for (var i in hashes) {
			var hashBlocker = hashes[i].value.split('@')[0];
			var hashBlocked = hashes[i].blocked_user_id;
			parsedHashes.push(hashBlocker);
			parsedHashes.push(hashBlocked);
		}

		return parsedHashes;
	}
};


UsersRepository.prototype.create = function (user) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		var hashedEmail = crypto.createHash('md5').update(user.email()).digest('hex');
		var md5Hash = crypto.createHash('md5').update('Zhanserik' + user.password()).digest('hex');
		var hashedPassword = crypto.createHash('md5').update(md5Hash).digest('hex');
		self.db.execute('INSERT INTO users (email, password, gender, platform, device_id, image_name, email_hash) ' +
				' VALUES ($1, $2, $3, $4, $5, $6, $7)' +
				' RETURNING id AS id',
			[user.email(), hashedPassword, user.gender(), user.platform(), user.deviceId(), user.imageName(), hashedEmail], function (err, rows) {
				if (err) {
					reject(err);
				} else {
					user.id(rows[0].id);
					user.imageSmallLink(self.cloudfront.host + '/images/small/' + user.imageName());
					user.imageBigLink(self.cloudfront.host + '/images/big/' + user.imageName());
					resolve(user);
				}
			});
	});

	return promise;
};

UsersRepository.prototype.updateDeviceInfo = function (userId, deviceId, platform) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET device_id=$1, platform=$2 WHERE id=$3', [deviceId, platform, userId], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve({'deviceId': deviceId});
			}
		});
	});
	return promise;
};


UsersRepository.prototype.processFiles = function (images, email) {

	var self = this;
	var milisecs = new Date().getTime().toString();
	var name = milisecs + '_' + crypto.createHash('md5').update(email).digest('hex') + '.jpeg';
	var pathToImages = path.join(__dirname + './../../public');
	var fileOptions = [
		{
			'binary': images.image,
			'path': pathToImages + '/big/' + name,
			'encoding': images.imageEncoding
		},
		{
			'binary': images.imageSmall,
			'path': pathToImages + '/small/' + name,
			'encoding': images.imageSmallEncoding
		}
	];
	var files = [
		promiseWriteFile(fileOptions[0].path, fileOptions[0].binary),
		promiseWriteFile(fileOptions[1].path, fileOptions[1].binary)
	];

	var promise = when.all(files).then(function (arr) {
		console.log("START UPLOADING");
		var start = new Date().getTime();
		self.uploadToS3(fileOptions[1].path, '/images/small/' + name, function (err, res) {
			if (err) {
				console.error(err);
			} else {
				return self.uploadToS3(fileOptions[0].path, '/images/big/' + name, function (err) {
					if (err) {
						console.error(err);
					} else {
						console.log("FINISH UPLOADING");
						var end = new Date().getTime();
						console.log((end - start)/ 1000);
						return name;
					}
				});
			}
		});

		return name;
	});

	return promise;
};


UsersRepository.prototype.uploadToS3 = function (filePath, s3Dir, callback) {
	var self = this;

	self.s3.putFile(filePath, s3Dir, { 'x-amz-acl': 'public-read' }, function (err, res) {
		if (err) {
			callback(err);
		} else {
			console.log("IMAGE UPLOADED TO S3");
			callback(null, res);
		}
	});
};


UsersRepository.prototype.setStatus = function (data, userId) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET status=$1 WHERE id=$2', [data.status, userId], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	});

	return promise;
};

UsersRepository.prototype.getMessageCounters = function(locations){
	var self = this;

	var promise = when.promise(function(resolve, reject){
		if (locations.toJSON() && locations.toJSON().length > 0) {

			var hashes = [];
			locations.map(function(loc){
				hashes.push('mc_' + loc.emailHash());
			});

			self.redis.MGET(hashes, function(err, results){
				if (err) {
					reject(err);
				} else {
					locations.map(function(loc, i){
						loc.messCount(results[i]);
					});
					resolve(locations);
				}
			});
		} else {
			resolve(locations);
		}
	});

	return promise;
};


UsersRepository.prototype.updateImageName = function (name, userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET image_name=$1 WHERE id=$2', [name, userId], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve({'big': self.cloudfront.host + '/images/big/' + name,
					'small': self.cloudfront.host + '/images/small/' + name});
			}
		});
	});

	return promise;
};


UsersRepository.prototype.isPasswordMatch = function (password, userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT * FROM users WHERE id=$1', [userId], function (err, rows) {
			if (err) {
				reject(err);
			} else if (rows[0].password === password) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
	return promise;
};

UsersRepository.prototype.changePassword = function (password, userId){
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET password=$2 WHERE id=$1', [userId, password], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
	return promise;
};


UsersRepository.prototype.getBlocked = function (userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {

		self.getById(userId).then(function (user) {
			self.db.execute('SELECT value, blocked_user_id FROM privacy_list_data WHERE blocked_user_id=$1 OR value=$2',
				[user.emailHash(), user.emailHash() + "@ec2-54-193-101-129.us-west-1.compute.amazonaws.com"], function (err, data) {
					if (err) {
						reject(err);
					} else {
						self.idsForHashes(self.parseHashes(data)).then(function (ids){
							resolve(ids);
						});
					}
				});
		}).catch(function (err) {
			reject(err);
		});
	});

	return promise;
};


UsersRepository.prototype.idsForHashes = function (hashes) {

	var self = this;
	var params = [];
	for (var i = 1; i <= hashes.length; i++) {
		params.push('$' + i);
	}
	var promise = when.promise(function (resolve, reject) {
		if (hashes && hashes.length > 0) {
			self.db.execute('SELECT id FROM users WHERE email_hash IN (' + params.join(',') + ')', hashes, function (err, rows) {
				if (err) {
					reject(err);
				} else {
					var ids = [];
					for (var i in rows){
						ids.push(rows[i].id);
					}
					resolve(ids);
				}
			});
		} else {
			resolve([]);
		}
	});

	return promise;
};


UsersRepository.prototype.isEmailExists = function (email) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT * FROM users WHERE email=$1', [email], function (err, rows) {
			if (err) {
				reject(err);
			} else if (rows && rows.length > 0) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
	return promise;
};


UsersRepository.prototype.getById = function (id) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT * FROM users WHERE id=$1', [id], function (err, rows) {
			if (err) {
				reject(err);
			} else {
				resolve(self.map(rows[0]));
			}
		});
	});

	return promise;
};

UsersRepository.prototype.getByHash = function (hash) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT * FROM users WHERE email_hash=$1', [hash], function (err, rows) {
			if (err) {
				reject(err);
			} else {
				resolve(self.map(rows[0]));
			}
		});
	});

	return promise;
};

var write = function move(fileOptions, callback) {
	fs.writeFile(fileOptions.path, fileOptions.binary, fileOptions.encoding, callback);
};


UsersRepository.prototype.getByKey = function (key) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.redis.SMEMBERS(key, function (err, data) {
			if (err) {
				reject(err);
			} else {
				var user;
				try {
					user = JSON.parse(data[0]);
					console.log("USER IN TRY");
					console.log(user);

				} catch (e) {
					if (data && data.length > 0) {
						var parsedArrayOfStrs = strutil.parse(data[0].split(']'));
						var arrayOfArrays = [];

						arrayOfArrays.push(strutil.parseStr(parsedArrayOfStrs[0].split('","')));
						arrayOfArrays.push(strutil.parseStr(parsedArrayOfStrs[1].split('","')));
						user = self.mapToJSON(arrayOfArrays);
						console.log(data);
						console.log("USER IN CATCH");
						console.log(user);

					} else {
						resolve(null);
					}
				}
				resolve(self.map(user));
			}
		});
	});

	return promise;
};

UsersRepository.prototype.deleteByKey = function (key) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.redis.DEL(key, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});

	return promise;
};

UsersRepository.prototype.createEmailKey = function (data) {
	var self = this;
	var emailKey = randomString({length: 40});
	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET email_key=$1 WHERE email=$2 RETURNING id AS id', [emailKey, data.email], function (err, rows) {
			if (err) {
				reject(err);
			} else {
				if (rows[0]) {
					resolve({'key': emailKey, 'id': rows[0].id});
				} else {
					resolve([]);
				}
			}
		});
	});
	return promise;
};


UsersRepository.prototype.setAvatar = function (icon, userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET icon_id=$1 WHERE id=$2', [icon, userId], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(icon);
			}
		});
	});

	return promise;
};

UsersRepository.prototype.getDeviceInfo = function (userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT platform, device_id FROM users WHERE id=$1', [userId], function (err, rows) {
			if (err) {
				reject(err);
			} else {
				resolve(rows[0]);
			}
		});
	});

	return promise;
};

UsersRepository.prototype.getDeviceInfoByHash = function(hash) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT platform, device_id FROM users WHERE email_hash=$1', [hash], function (err, rows) {
			if (err) {
				reject(err);
			} else {
				resolve(rows[0]);
			}
		});
	});

	return promise;
};

UsersRepository.prototype.reset = function (data, userId) {
	var self = this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('UPDATE users SET password=$1 WHERE id=$2', [data.password, userId], function (err) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});

	return promise;
};

UsersRepository.prototype.setKey = function (key, user) {
	var self = this;
	var promise = when.promise(function (resolve, reject) {
		self.redis.DEL(key, function (err) {
			if (err) {
				reject(err);
			} else {
				self.redis.SADD(key, JSON.stringify(user.toJSON()), function (err) {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				});
			}
		});
	});

	return promise;
};

UsersRepository.prototype.getBlockListFor = function (user) {
	var self =this;

	var promise = when.promise(function (resolve, reject) {
		self.db.execute('SELECT value FROM privacy_list_data WHERE blocked_user_id=$1', [user.emailHash()], function(err, data){
			if (err) {
				reject(err);
			} else {
				self.idsForHashes(self.parseHashes(data)).then(function (ids){
					resolve(ids);
				});
			}
		});
	});

	return promise;
};


module.exports = UsersRepository;