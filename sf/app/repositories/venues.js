/**
 * Created by springfruit on 17.04.14.
 */

var request  = require('request');
var Venue    = require('../models/venue');
var when     = require('when');
var Collection = require('../utils/collection');

var VenuesRepository = function(){
  this.dependencies   = {
    'db' : 'modules.db'
  };
	this.map = function(json) {

		var result  = new Collection();
		for (var i in json) {
			result.push(Venue.create(json[i]));
		}
		return result;
	};
};


VenuesRepository.prototype.save = function(venue, callback){
	var self = this;

	self.db.execute('INSERT INTO venues ' +
			'(id, name, phone, address, lat, lng, city, state, country, category_id, category_name)' +
			'VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)',
		[venue.id(), venue.name(), venue.phone(), venue.address(), venue.lat(), venue.lng(),
			venue.city(), venue.state(), venue.country(), venue.categoryId(),
			venue.categoryName()], function(err) {
			if (err) {
				callback(err);
			} else {
				callback(null, venue);
			}
		});
};

VenuesRepository.prototype.getById = function(id) {
	var self = this;

	var promise = when.promise(function(resolve, reject) {
		if (id === undefined) {
			resolve(null);
		} else if (id && typeof id === 'string') {
			self.db.execute('SELECT * FROM venues WHERE id=$1', [id], function(err, rows) {
				if (err) {
					reject(err);
				} else if (rows && rows.length !== 0) {
					var venue = Venue.create(rows[0]);
					resolve(venue);
				} else {
					resolve(null);
				}
			});
		} else if (typeof id === 'object' && id.length > 0){
			var params = [];
			for(var i = 1; i <= id.length; i++) {
				params.push('$'+i);
			}

			self.db.execute('SELECT * FROM venues WHERE id IN (' +  params.join(',') +')', id, function(err, rows) {
				if (err) {
					reject(err);
				} else if (rows && rows.length !== 0){
					resolve(self.map(rows));
				} else {
					resolve(null);
				}
			});

		} else {
			resolve(null);
		}
	});
	return promise;

};

VenuesRepository.prototype.getVenueInfo = function(id, callback){
	console.log("GET VENUE INFO");
	var clientId        = 'OLBGCWPFQCX1RPU0LXZR44OWARXGIVZGL2RHW5Q3340K02J4';
	var clientSecret    = 'KNQ0IYIMUPUDJLGSQBBVROFE0J1DW3SQ5A24AGJDP2XVC5S2';
	var url             = 'https://api.foursquare.com/v2/venues/';
	var fullAddress = url + id + '?client_id=' + clientId + '&client_secret=' + clientSecret + '&v=' + getFormatedDate();
	var options = {
		'url': fullAddress,
		'headers': {
			'Accept-Language': 'en'
		}
	};

	request(options, function (err, res, body) {
		var data = JSON.parse(body).response.venue;
		var venue = Venue.create();
		venue.id(data.id);
		venue.name(data.name);
		venue.phone(data.contact.formattedPhone);
		venue.address(data.location.address);
		venue.lat(data.location.lat);
		venue.lng(data.location.lng);
		venue.city(data.location.city);
		venue.state(data.location.state);
		venue.country(data.location.country);
		if (data.categories && data.categories.length > 0) {
			venue.categoryId(data.categories[0].id);
			venue.categoryName(data.categories[0].name);
		}

		venue.validate().then(function () {
			if (venue.isValid) {
				callback(null, venue);
			} else {
				callback(venue.errors);
			}
		});
	});
};


function validateMonthOrDate(monthOrDate) {
	if (monthOrDate.length === 1) {
		return '0' + monthOrDate;
	} else {
		return monthOrDate;
	}
}

function getFormatedDate() {
	var date = new Date();
	var currDate = validateMonthOrDate(date.getDate().toString());
	var currMonth = validateMonthOrDate((date.getMonth() + 1).toString());
	var currYear = date.getFullYear().toString();
	return currYear + currMonth + currDate;
}


module.exports = VenuesRepository;