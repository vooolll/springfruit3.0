/**
 * Created by springfruit on 29.04.14.
 */
var UserController  = {

	signup: function(req, res){
		var data = {};
		data.imagesInfo = {};
		req.pipe(req.busboy);
		req.busboy.on('file', function(fieldname, file, filename, encoding){
			file.on('data', function(binary) {
				data.imagesInfo[fieldname + 'Encoding'] = encoding;
				if(!data.imagesInfo[fieldname]) {
					data.imagesInfo[fieldname]  = new Buffer(binary);
				} else {
					data.imagesInfo[fieldname]  = Buffer.concat([data.imagesInfo[fieldname], binary]);
				}
			});
		});

		req.busboy.on('field', function(fieldname, val){
			data[fieldname] = val;
		});

		req.busboy.on('finish', function() {
			console.log("---SIGN IN DATA---");
			console.log(data);
			console.log("------------------");
			req.services.users.create(data).then(function(user){
				res.json(user);
			}).catch(function(err){
				console.log(err);
				res.json({ 'error' : err });
			});
		});
	},

	changeImage: function(req, res) {
		var data = {};
		data.imagesInfo = {};
		req.pipe(req.busboy);
		req.busboy.on('file', function(fieldname, file, filename, encoding){
			file.on('data', function(binary) {
				data.imagesInfo[fieldname + 'Encoding'] = encoding;

				if(!data.imagesInfo[fieldname]) {
					data.imagesInfo[fieldname] = new Buffer(binary);
				}
				else {
					data.imagesInfo[fieldname] = Buffer.concat([data.imagesInfo[fieldname], binary]);
				}
			});
		});
		req.busboy.on('finish', function() {
			req.services.users.changeImage(data, req.user, req.query.sid).then(function(link) {
				res.json({'image_big_link': link.big, 'image_small_link': link.small});
			}).catch(function(err){
				console.log(err);
				res.json({'error': err});
			});
		});
	},

	getByKey: function(req, res) {
		req.services.users.getByKey(req.params.sid).then(function(data){
			res.json(data);
		}).catch(function(err) {
			res.json({'error': err});
		});
	},

	setStatus: function(req, res) {
		var data = req.body;

		req.services.users.setStatus(data, req.user, req.query.sid).then(function(data){
			res.json({'error': false, 'description': 'success'});
		}).catch(function(err){
			res.json({'error' : err});
		});
	},

	setAvatar: function(req, res){
		req.services.users.setAvatar(req.body, req.user,  req.query.sid).then(function(){
			res.json({'error': false, 'description': 'success'});
		});
	},


	forgot: function(req, res){
		req.services.users.forgotPassword(req.body).then(function(key){
			res.json({'error': 'false', 'description': 'success'});
		}).catch(function(err){
			res.json({'error' : err});
		});
	},

	checkEmail: function(req, res){
		req.services.users.checkEmail(req.body.email).then(function(isExist){
			if (!isExist){
				res.json({'error': false, 'description': 'success'});
			} else {
				res.statusCode = 400;
				res.json({'error': true, 'code': 4, 'description': 'user exists'});
			}
		});
	},

	changePassword: function(req, res){
		req.services.users.changePassword(req.body, req.user.id()).then(function(result){
			if (!result.error) {
				res.json(result);
			} else {
				res.status(400).json(result);
			}
		}).catch(function(err){
			res.json(err);
		});
	},

	checkKey: function(req, res){
		req.services.users.checkKey(req.params.key, req.params.uid).then(function(allowed){
			console.log(allowed);
			if (allowed){
				res.render('reset.ejs',  {id: req.params.uid});
				//show form
			} else {
				res.send('Ooooops');
			}
		}).catch(function(err){
			res.json({ 'error': err });
		});
	},

	reset: function(req, res){
		req.services.users.reset(req.body, req.params.uid).then(function(result){
			res.render('result.ejs', result);
		}).catch(function(err){
//			console.log(err);
			res.json({ 'error': err });
		});
	},

	blockList: function(req, res){
		req.services.users.blockList(req.user).then(function(blockList){
			res.json({'block': blockList});
		}).catch(function(err){
			console.log(err);
			res.json({'error': err});
		});
	},

	getById: function(req, res){
		req.services.users.getById(req.params.id).then(function(user){
			res.json(user);
		}).catch(function(err){
			res.json({'error': err});
		});
	},

	getByHash: function(req, res){
		req.services.users.getByHash(req.params.hash).then(function(user){
			res.json(user);
		}).catch(function(err){
			res.json({'error': err});
		});
	},

	offlineMessages: function(req, res){
		req.services.users.sendOffline(req.body).then(function(){
			res.json({'error': false, 'description': 'success'});
		}).catch(function(err){
			console.log(err);
			res.json({'error': err});
		});
	}

};

module.exports = UserController;