/**
 * Created by springfruit on 21.05.14.
 */
var LinkupController = {

	add: function(req, res){
		req.services.linkups.add(req.user.id(), req.body.another_user_id).then(function(data){
			if (!data.error){
				res.json(data);
			} else {
				res.status(400).json(data);
			}


		}).catch(function(err){
			console.log(err);
			res.json({'error': err});
		});
	},

	decision: function(req, res){
		req.services.linkups.accept(req.user.id(), req.query.accept).then(function(){
			res.json({'error': false, 'description': 'success'});
		}).catch(function(err){
			res.json({'error': err});
		});
	},

	del: function(req, res){
		req.services.linkups.del(req.user.id()).then(function(){
			res.json({'error': false, 'description': 'success'});
		}).catch(function(err){
			res.json({'error': err});
		});
	}

};

module.exports = LinkupController;