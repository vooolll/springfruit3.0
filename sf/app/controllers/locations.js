var LocationController  = {

	getLocation : function(req, res)
	{
		var loc = {'lat': req.query.lat, 'lng': req.query.lng };
		req.services.locations.getLocation(loc, req.user.id()).then(function(locations){
			console.log("response");
			console.log(locations.toJSON());
			res.json({'locations': locations.toJSON()});
		}).catch(function(err) {
			console.log(err);
			res.json({'error': err});
		});
	},

	postLocation : function(req, res)
	{
		req.services.locations.save(req.body, req.user).then(function(location){
			res.json({'location' : location});
		}).catch(function(err){
			console.log(err);
			res.json({'error' : err});
		});
	},

	getStats : function(req, res)
	{
		req.services.locations.getStats(req.query.city).then(function(data){
			res.json({'stats' : data});
		}).catch(function(err){
			console.log(err);
			res.json({'error' : err});
		});
	},

	logout: function(req, res)
	{
		req.services.locations.logout(req.user, req.query.sid).then(function(){
			res.json({'error': false, 'description': 'success'});
		}).catch(function(err){
			res.json({'error': err});
		});
	}
};

module.exports  = LocationController;