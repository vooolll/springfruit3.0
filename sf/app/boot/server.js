var path    = require('path');
var express = require('express');
var helper  = require(__dirname + '/../utils/helper');
var app     = express();
var when    = require('when');
var busboy  = require('connect-busboy');

process.on('uncaughtException', function (err) {
	console.error('Caught exception: ' + err);
});

var deps    = require(__dirname + '/dependencies');
deps.then(function(dependencies) {

	app.use(express.logger('dev'));
	app.use(express.json());
	app.use(express.urlencoded());
	app.use(busboy());
	app.set('views', path.join(__dirname + '/../templates'));
	app.use(express.static(path.join(__dirname +'./../../public')));


	app.use(function(req, res, next) {
		req.repositories    = dependencies.repositories;
		req.services        = dependencies.services;
		return next();
	});



	var dir             = __dirname + '/../controllers';
	var files           = helper.requireDir(dir);
	var controllers     = {};
	files.forEach(function(f){
		var ctrl    = f.obj;
		var key     = path.basename(f.file, '.js');

		controllers[key] = ctrl;
	});

	var routes  = require(__dirname + '/../routes.js');
	routes(app, controllers);

	app.listen(3000);
});



//module.exports  = app;

module.exports  = deps;