var helper  = require(__dirname + '/../utils/helper');
var when    = require('when');
var keys    = require('when/keys');

//NODE_ENV=production node app.js
var env    = process.env.NODE_ENV || 'dev';



try {
	var config  = require(__dirname + '/../config/' + env + '.js');
}
catch (err) {
	console.log('config file "' + __dirname + '/../config/' + env + '.js" not found');
	process.exit(1);
}



var modules         = helper.requireDir(__dirname + '/../' + 'modules');
var modulesPromises = {};
modules.forEach(function(hook){
	if(hook.obj && (typeof hook.obj) == 'function') {
		modulesPromises[hook.key]  = hook.obj(config);
	}
});

var promise = when.promise(function(resolve, reject, notify) {
	var depsPromise = keys.all(modulesPromises);



	depsPromise.then(function(res){
		var dependencies    = {
			'modules'       : res,
			'repositories'  : {},
			'services'      : {}
		};

		var files           = helper.requireDir(__dirname + '/../repositories');

		files.forEach(function(file){
			var repo = new file.obj;

			helper.attachDependencies(repo, dependencies);
			dependencies.repositories[file.key] = repo;
		});


		var files    = helper.requireDir(__dirname + '/../services');
		files.forEach(function(file){
			var service = new file.obj;

			helper.attachDependencies(service, dependencies);
			dependencies.services[file.key] = service;
		});

		console.log('dependecies loaded');
		resolve(dependencies);
	});
});


//var promise = keys.all(modulesPromises);
////
//promise.then(function(res){
//	var dependencies    = {
////		'modules'       : res,
//		'repositories'  : {},
//		'services'      : {}
//	};
//
//
////	dependencies.modules    = res;
//
//	var files           = helper.requireDir(__dirname + '/../repositories');
//	var repositories    = {};
//	files.forEach(function(file){
//		var repo = new file.obj;
//
//
//
//		helper.attachDependencies(repo, dependencies);
//		dependencies.repositories[file.key] = repo;
//
//		repositories[file.key]  = repo;
//	});
//
////	dependencies.repositories    = repositories;
////	dependencies.repositories    = 'sadddjsdasldjas;djask;d';
//
//
//
//
////	return dependencies;
//	return 'test';
//}).then(function(d){
//	console.log('----------------test------------------');
//	console.log(d);
//	console.log('----------------test------------------');
//});

//
//var repositories    = helper.requireDir(__dirname + '/../repositories');
//repositories.forEach(function(file){
//	var repo = new file.obj;
//
//	helper.attachDependencies(repo, dependencies);
//	dependencies.repositories[file.key] = repo;
//});
//
//var services    = helper.requireDir(__dirname + '/../services');
//services.forEach(function(file){
//	var service = new file.obj;
//
//	helper.attachDependencies(service, dependencies);
//	dependencies.services[file.key] = service;
//});


module.exports  = promise;