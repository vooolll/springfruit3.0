var argv            = require('optimist').argv;
var helper          = require(__dirname + '/../utils/helper');
var dependencies    = require(__dirname + '/../boot/dependencies');

var run = function(deps) {
	var files       = helper.requireDir(__dirname + '/..' + '/commands');
	var commands    = {};

//	console.log('----------------cli------------------');
//	console.log(deps);
//	console.log('----------------cli------------------');


	files.forEach(function(file){
		commands[file.key]   = file.obj;
	});


	var name    = argv._[0];
	if(!name) {
		console.log(
			'command not set.' + "\n" +
				'usage: sf command_name'
		);

		throw new Error('command name');
	}
	else {
		var command = commands[name];

		if(!command) {
			console.log('command "' + name + '" not found');
			process.exit(1);
		}

		command.run(argv, deps);
	}
};

//dependencies.then(function(test){
//	console.log(test
//	);
//});

//console.log(dependencies);
//console.log('olololo');
//process.exit(1);

//module.exports.run  = run;
dependencies.then(run);

module.exports  = dependencies;