/**
 * Created by springfruit on 17.05.14.
 */
var authOnly = function(req, res, next) {
	if (req.query.sid) {

		req.services.users.getByKey(req.query.sid).then(function(user){
			if (user && user.id) {
				req.user = user;
				next();
			} else {
				res.statusCode = 403;
				res.json({'access': 'Forbidden'});
			}
		}).catch(function(err){
			res.statusCode = 500;
			res.json({'error': err});
		});
	} else {
		res.statusCode = 403;
		res.json({'access': 'Forbidden'});
	}
};

module.exports = authOnly;