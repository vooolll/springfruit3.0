#!/bin/bash
curl -XDELETE 'http://localhost:9165/locations' && echo
curl -XPUT 'http://localhost:9165/locations' && echo
curl -XPUT 'http://localhost:9165/locations/location/_mapping' -d '
{
    "location": {
        "properties": {
            "location": {"type" : "geo_point"},
            "gender" : {"type" : "boolean"},
            "last_appearance": {"type": "long"},
            "city": { type: "string", index: "not_analyzed" }
        }
    }
}'  && echo "Mapping done"
#curl -XPOST 'http://localhost:9165/locations/location/_refresh' && echo