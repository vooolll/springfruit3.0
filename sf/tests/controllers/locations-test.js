/**
 * Created by springfruit on 12.04.14.
 */
var assert = require('assert');
var request = require('supertest');

request = request('localhost:3000');

suite('Location routes', function () {
  suite('POST /api/location', function () {
    test('should be 200, and have ct app/json', function (done) {
      var postData = {
        'location': {'lat': 40,
          lng: 10
        },
        'category_id': 5,
        'gender': true,
        'city': 'Almaty'
      };
      request.post('/api/location')
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });

  suite('GET /api/location', function() {
    test('should be 200, and have ct app/json', function(done) {
      request.get('/api/location')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});

