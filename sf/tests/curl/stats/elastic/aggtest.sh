#!/bin/sh

curl -X GET http://localhost:9165/locations/location/_search -d '
{
	"size" : 0,
	"aggregations" : {
		"cities" : {
			"filter" : {
				"term" : {
					"city": "Almaty"
				}
			},
			"aggregations": {
				"lasts" : {
					"filter" : {
                        "range" : {
                            "last_appearance" : {
                                "gte" : 100
                            }
                        }
                    },
                    "aggregations": {
						"venues" : {
	                        "terms" : { "field" : "venue_id"},
	                        "aggregations": {
	                            "genders" : {
	                                "terms" : {"field" : "gender"}
	                            }
	                        }
	                    }
	                }
				}
			}
    	}
    }
}
'| python -mjson.tool