#!/bin/bash
curl -XDELETE 'http://localhost:9165/locations' && echo
curl -XPUT 'http://localhost:9165/locations' && echo
curl -XPUT 'http://localhost:9165/locations/location/_mapping' -d '
{
    "location": {
        "properties": {
            "location": {"type" : "geo_point"},
            "gender" : {"type" : "boolean"},
            "last_appearance": {"type": "long"},
            "city": { type: "string", index: "not_analyzed" }
        }
    }
}'  && echo "Mapping done"


echo "SAVING DATA TO INDEX"
curl -XPUT 'http://localhost:9165/locations/location/1' -d '{
   "location":
   {
    "lat": "47.785346",
    "lon": "67.701857"
   },
    "category_id": "5",
    "gender": "true",
    "city": "Almaty",
    "last_appearance": "40",
    "venue_id": "5229774711d2a3ec9e333d00",
    "platform": "0",
    "device_id": "dasf213231321"
}'

curl -XPUT 'http://localhost:9165/locations/location/2' -d '{
   "location":
   {
    "lat": "43.785346",
    "lon": "12.701857"
   },
    "category_id": "4",
    "gender": "false",
    "city": "Almaty",
    "last_appearance": "1000",
    "venue_id": "5229774711d2a3ec9e333d00",
    "platform": "0",
    "device_id": "dasf213231321"
}'

curl -XPUT 'http://localhost:9165/locations/location/3' -d '{
   "location":
   {
    "lat": "43.785346",
    "lon": "12.701857"
   },
    "category_id": "4",
    "gender": "false",
    "city": "Almaty",
    "last_appearance": "1544",
    "venue_id": "5229774711d2a3ec9e333d00",
    "platform": "0",
    "device_id": "dasf213231321"
}'

curl -XPUT 'http://localhost:9165/locations/location/4' -d '{
   "location":
   {
    "lat": "47.785346",
    "lon": "67.701857"
   },
    "category_id": "5",
    "gender": "true",
    "city": "Almaty",
    "last_appearance": "150",
    "venue_id": "5229774711d2a3ec9e333d00",
    "platform": "0",
    "device_id": "dasf213231321"
}'

curl -XPUT 'http://localhost:9165/locations/location/5' -d '{
   "location":
   {
    "lat": "47.785346",
    "lon": "67.701857"
   },
    "category_id": "5",
    "gender": "true",
    "city": "Almaty",
    "last_appearance": "300",
    "venue_id": "5229774711d2a3ec9e333d00",
    "platform": "0",
    "device_id": "dasf213231321"
}'
