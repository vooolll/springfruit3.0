#!/bin/sh

curl -X GET http://localhost:9165/locations/location/_search -d '
{
	"size" : 0,
	"aggregations" : {
		"lasts" : {
			"filter" : {
				"range" : {
					"last_appearance" : {
						"gte" : 300
					}
				}
			},
			"aggregations": {
				"cities" : {
					"filter" : {
						"term" : {"field" : "city"}
					},
					"aggregations" : {
						"terms" : { "field" : "venue_id"},
						"aggregations": {
							"terms" : {"field" : "gender"}
						}
					}
				}
			}
    	}
    }
}
'| python -mjson.tool