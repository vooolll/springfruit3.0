#!/bin/bash
curl -X GET http://localhost:9165/locations/location/_search -d '
{
				"query": {
					"match_all": {}
				},
				"filter": {
					"and": [
						{
							"or": [
								{
									"geo_distance": {
										"distance": "40km",
										"location": {
										  "lat": "47.785346",
                      "lon": "67.701857"
										}
									}
								},
								{
									"ids": {
										"values": "3"
									}
								}
							]
						},
						{
							"range": {
								"last_appearance": {
									"gte": "100"
								}
							}
						},
						{
							"not": {
								"filter": {
									"ids": {
										"values": "idsBlock"
									}
								}
							}
						}
					]
				}
			}
		}
'