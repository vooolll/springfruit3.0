#!/bin/bash
curl -X GET http://localhost:9165/locations/location/_search -d '
{
  "filter": {
    "or": [
      {
        "ids" : {
          "values" : ["1"]
        }
      },
      {
        "and": [
          {
            "geo_distance": {
              "distance": "40km",
              "location": {
                "lat": "47.785346",
                "lon": "67.701857"
              }
            }
          },
          {
            "range": {
              "last_appearance" : {
						    "gte" : "100"
					    }
            }
          },

        ]
      }

    ]
  }
}
'| python -mjson.tool