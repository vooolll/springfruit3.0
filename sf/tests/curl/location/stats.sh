#!/bin/bash
curl -X GET http://localhost:9165/locations/location/_search -d '
{
	"filter" : {
	  "and" : [
	    {
	      "range" :
	      {
	        "last_appearance" :
	        {
		        "gte" : "2222222222222222222"
		      }
	      }
	    },
	    {
		    "term" : {"city" : "Almaty"}
	    }
	  ]
	},

    "aggs" : {
	    "venues" : {
			"terms" : { "field" : "venue_id"},
			"aggs": {
                "genders" : {
                    "terms" : {"field" : "gender"}
                }
            }
		}
	}
}' | python -mjson.tool