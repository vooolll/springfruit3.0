/**
 * Created by springfruit on 17.04.14.
 */

var assert = require('assert');
var deps = require('../../app/boot/dependencies');
var service = deps.services.venues;

var data = {
  "id":"5229774711d2a3ec9e333d00",
  "name":"Manga Sushi",
  "contact":{
    "phone":"+77019528000",
    "formattedPhone":"+7 701 952 8000"
  },
  "location":{
    "address":"ул. Гоголя, 201",
    "crossStreet":"уг. ул. Джумалиева",
    "lat":43.25759570959907,
    "lng":76.91199901042022,
    "distance":472,
    "cc":"KZ",
    "city":"Almaty",
    "state":"Almaty Qalasy",
    "country":"Kazakhstan"
  },
  "categories":[
    {
      "id":"4bf58dd8d48988d1d2941735",
      "name":"Sushi Restaurant",
      "pluralName":"Sushi Restaurants",
      "shortName":"Sushi",
      "icon":{
        "prefix":"https:\/\/ss1.4sqi.net\/img\/categories_v2\/food\/sushi_",
        "suffix":".png"
      },
      "primary":true
    }
  ],
  "verified":false,
  "stats":{
    "checkinsCount":1190,
    "usersCount":583,
    "tipCount":61
  },
  "url":"http:\/\/www.manga-sushi.kz",
  "specials":{
    "count":0,
    "items":[
    ]
  },
  "hereNow":{
    "count":1,
    "summary":"1 person here",
    "groups":[
      {
        "type":"others",
        "name":"Other people here",
        "count":1,
        "items":[
        ]
      }
    ]
  },
  "referralId":"v-1397048958"
}


suite('Services', function () {
  suite('#save', function() {
    test('should save in db', function(done) {
      service.save(data, function(err, venue) {
        if (err) {
          return done(err);
        } else {
          console.log(venue);
          done();
        }
      });
    });
  });
	suite('#getById', function() {
		test('get by id from db', function(done) {
			service.getById('5229774711d2a3ec9e333d00', function(err, venue) {
				if (err) {
					return done(err);
				} else {
					console.log(venue);
					done();
				}
			});
		});
	});

	suite('#getVenueInfo', function() {
		test('should return forsquare info', function(done) {
			service.getVenueInfo('5229774711d2a3ec9e333d00', function(err, venue) {
				console.log(venue);
				done();
			});
		});
	});
});