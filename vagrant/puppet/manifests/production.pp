class apt_update {
exec { "aptGetUpdate":
  command => "sudo apt-get update",
  path => ["/bin", "/usr/bin"]
}
}

class othertools {
package { "git":
  ensure => latest,
  require => Exec["aptGetUpdate"]
}

package { "vim-common":
  ensure => latest,
  require => Exec["aptGetUpdate"]
}

package { "curl":
  ensure => present,
  require => Exec["aptGetUpdate"]
}

package { "htop":
  ensure => present,
  require => Exec["aptGetUpdate"]
}
}

class node-js {
include apt
apt::ppa {
  'ppa:chris-lea/node.js': notify => Package["nodejs"]
}

package { "nodejs" :
  ensure => latest,
  require => [Exec["aptGetUpdate"],Class["apt"]]
}

exec { "npm-update" :
  cwd => "/vagrant",
  command => "npm -g update",
  onlyif => ["test -d /vagrant/node_modules"],
  path => ["/bin", "/usr/bin"],
  require => Package['nodejs']
}

}



class { 'deploy::deploy-nginx':
  server_name     => 'ec2-54-193-101-129.us-west-1.compute.amazonaws.com',
  www_root        => '/var/www/sf/sf/public'
}

class { 'redis': }
class { 'deploy::elastic': }


class { 'deploy::database':
  postgres_password   => 'loveyourmom2048',
  database            => 'spring_fruit_production',
  user                => 'sf',
  password            => 'loveyourdaughter112358',
}

class { '::rabbitmq':
  port  => '5672'
}

class supervisor {
supervisor::program { 'node-app':
  ensure      => present,
  enable      => true,
  command     => '/home/valeratest/springfruit3.0/sf/server.js',
  directory   => '/home/valeratest/springfruit3.0/sf',
  environment => 'NODE_ENV=production',
  user        => 'valeratest',
  group       => 'valeratest',
  logdir_mode => '0770',
} }





include apt_update
include othertools
include node-js
include redis
include '::rabbitmq'