class apt_update {
exec { "aptGetUpdate":
  command => "sudo apt-get update",
  path => ["/bin", "/usr/bin"]
}
}

class othertools {
package { "git":
  ensure => latest,
  require => Exec["aptGetUpdate"]
}

package { "vim-common":
  ensure => latest,
  require => Exec["aptGetUpdate"]
}

package { "curl":
  ensure => present,
  require => Exec["aptGetUpdate"]
}

package { "htop":
  ensure => present,
  require => Exec["aptGetUpdate"]
}
}

class node-js {
include apt
apt::ppa {
  'ppa:chris-lea/node.js': notify => Package["nodejs"]
}

package { "nodejs" :
  ensure => latest,
  require => [Exec["aptGetUpdate"],Class["apt"]]
}

exec { "npm-update" :
  cwd => "/vagrant",
  command => "npm -g update",
  onlyif => ["test -d /vagrant/node_modules"],
  path => ["/bin", "/usr/bin"],
  require => Package['nodejs']
}



exec { "sails-install" :
  cwd => "/vagrant",
  command => "npm -g install supervisor",
  path => ["/bin", "/usr/bin"],
  require => Package['nodejs']
}
}

class graphicsmagick {
package { "graphicsmagick":
  ensure => latest
}
}

class { 'deploy::deploy-nginx':
  server_name     => 'sf.lcl',
  www_root        => '/var/www/sf/sf/public/'
}

class { 'redis': }
class { 'deploy::elastic': }


class { 'deploy::database':
  postgres_password   => '12345',
  database            => 'spring_fruit_test',
  user                => 'sf',
  password            => '12345',
}

class { '::rabbitmq':
  port  => '5672'
}


include apt_update
include othertools
include node-js
include redis
include '::rabbitmq'