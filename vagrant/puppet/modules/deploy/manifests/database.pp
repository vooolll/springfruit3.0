class deploy::database (
	$postgres_password  = undef,
    $user               = undef,
    $password           = '',
    $database           = ''
) {
	include apt

	class { 'postgresql::globals':
		version             => '9.3',
		manage_package_repo => true,
	} ->
	class { 'postgresql::server':
	    version                     => '9.3',
	    postgres_password           => $postgres_password,
	    listen_addresses            => '*',
	    ip_mask_deny_postgres_user  => '0.0.0.0/32',
	    encoding                    => 'UTF8',
	    locale                      => 'en_US.UTF-8',
	    ipv4acls              => [
	      'local      all   all                       trust',
	      'host       all   all     0.0.0.0/0         md5',
	      'hostssl    all   all     0.0.0.0/0         md5'
	    ],
	}

	class {'postgresql::lib::devel' : }

	postgresql::server::db { $database:
		user      => $user,
		password  => postgresql_password($user, $password),
	}

	# TODO Move to params, set from node manifest
	postgresql::server::config_entry {
	    'port'                          : value => 5432;
	    'max_connections'               : value => 30;
	    'shared_buffers'                : value => '16MB';
	    'logging_collector'             : value => 'on';
	    'log_rotation_size'             : value => 0;
	    'log_min_duration_statement'    : value => 300;
	}

}