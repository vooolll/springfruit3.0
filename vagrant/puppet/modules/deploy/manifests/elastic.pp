class deploy::elastic () {

  include java

  class { 'elasticsearch':
    package_url               => 'https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.0.0.deb',
    restart_on_change         => true,
    config                    => {
      'cluster'               => {
        'name'                => 'lx.Jae5echaeMaev7ejahy4ai0dizo5mieR',
      },
      'node'                  => {
        'name'                => 'lx.elasticsearch01',
      },
      'http'                  => {
        'enabled'             => true,
        'port'                => 9165,
      },
#      'http.type'            => 'com.sonian.elasticsearch.http.jetty.JettyHttpServerTransportModule',
#      'sonian.elasticsearch.http.jetty' => {
#        'port'               => '9165',
#        'config'             => 'jetty.xml,jetty-hash-auth.xml,jetty-restrict-all.xml',
#      },
      'index'                => {
        'number_of_replicas' => '0',
        'number_of_shards'   => '1'
      },
#      'marvel.agent.enabled' => false,
      'marvel.agent.exporter.es.hosts' => ['localhost:9165'],
    },
    require                  => Class['java'],
  }

#  elasticsearch::plugin { 'imotov/elasticsearch-analysis-morphology':
#    url        => 'http://dl.bintray.com/content/imotov/elasticsearch-plugins/elasticsearch-analysis-morphology-1.1.0.zip',
#    module_dir => 'morph',
#  }

  elasticsearch::plugin { 'elasticsearch/marvel/latest':
    module_dir => 'marvel',
  }

#  elasticsearch::plugin { 'elasticsearch-jetty':
#    url        => 'https://oss-es-plugins.s3.amazonaws.com/elasticsearch-jetty/elasticsearch-jetty-0.90.0.zip',
#    module_dir => 'jetty',
#    before     => File['/etc/elasticsearch/real.properties', '/etc/elasticsearch/config'],
#  }
#
#  file { '/etc/elasticsearch/real.properties':
#    ensure      => present,
#    source      => 'puppet:///modules/legalex/elasticsearch/realm.properties',
#  }
#
#  file { '/etc/elasticsearch/config':
#    source      => 'puppet:///modules/legalex/elasticsearch/config',
#    recurse     => true,
#  }

}
