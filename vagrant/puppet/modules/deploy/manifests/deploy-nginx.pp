class deploy::deploy-nginx (
	$server_name        = 'localhost',
    $user               = 'root',
    $www_root           = '/usr/share/nginx/html'
) {
	include concat::setup

	class { 'nginx':
	    process_user                    => $user,
	    worker_connections              => 1024,
	    types_hash_max_size             => 2048,
	    server_names_hash_bucket_size   => 128,
	    keepalive_timeout               => 65,
	    sendfile                        => 'off',
	    template                        => 'deploy/conf.d/nginx.conf.erb',
	}

	nginx::resource::vhost { $server_name:
	    ensure              => present,
	    fastcgi             => present,

	    owner               => $user,

	    www_root            => $www_root,
	    template_header     => 'deploy/vhost/vhost_header.erb',
	    template_directory  => 'deploy/vhost/vhost_location_index.erb',
	    template_fastcgi    => 'deploy/vhost/vhost_fastcgi.erb',
	}

}